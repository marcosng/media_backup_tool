from appJar import gui
import datetime
from func import connect_to_host, create_backup_file, download_file


HOST = 'reconquista.claro.amx'
PORT = 22
USERNAME = 'root'
PASSWORD = 'c3r4g0nadmin'
SOURCE = '/app/docker/volumes/infra_media/_data'
FILENAME = '/root/infra_media_backup_{}.tgz'.format(datetime.datetime.today().strftime('%Y.%m.%d_%H.%M.%S'))


def press(button):
    if button == '...':
        file = app.directoryBox(title="Backup file folder")
        app.setEntry('Output dir:', file)

    elif button == 'Run':
        folder = app.getEntry('Output dir:')
        connect_status, ssh = connect_to_host(HOST, PORT, USERNAME, PASSWORD)

        if connect_status == 'OK!':
            app.setStatusbar("Connection: OK!", 0)
            app.setStatusbarBg("green", field=0)
            app.topLevel.update()

            backup_status = create_backup_file(ssh, SOURCE, FILENAME)
            if backup_status == 'OK!':
                app.setStatusbar("Back up: OK!", 1)
                app.setStatusbarBg("green", field=1)
                app.topLevel.update()

                app.setStatusbar("Downloading...", 2)
                app.setStatusbarBg("orange", field=2)
                app.topLevel.update()
                download_status = download_file(ssh, FILENAME, folder)
                if download_status == 'OK!':
                    app.setStatusbar("Download: OK!", 2)
                    app.setStatusbarBg("green", field=2)
                    app.topLevel.update()

                    app.infoBox("Info", "Backup complete!")
                    exit()


app = gui("Infra_media Backup Creator", "390x110")
app.addLabelEntry("Output dir:", 1, 1)
app.addButton("...", press, 1, 2)
app.addButton("Run", press, 2, 1, 2)
app.addStatusbar(fields=3)
app.setStatusbar("Connection: - ", 0)
app.setStatusbar("Back up: - ", 1)
app.setStatusbar("Download: - ",2)
app.setFont(12)
app.go()