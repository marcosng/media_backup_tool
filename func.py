import paramiko
import warnings

warnings.simplefilter('ignore')


def connect_to_host(HOST, PORT, USERNAME, PASSWORD):
    print('Connecting to {} as {}... '.format(HOST, USERNAME), flush=True, end='')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(HOST, PORT, USERNAME, PASSWORD)
        print('OK!')
        return 'OK!', ssh
    except:
        print('NOT OK!')
        return 'error', None


def create_backup_file(ssh, SOURCE, FILENAME):
    print('Compressing database... ', flush=True, end='')
    try:
        ssh.exec_command('tar -zcvf {} -C {} .'.format(FILENAME, SOURCE))
        print('OK!')
        return 'OK!'
    except:
        print('NOT OK!')
        return 'error'


def download_file(ssh, FILENAME, DESTINATION):
    print('Downloading file... ', flush=True, end='')
    ftp = ssh.open_sftp()
    try:
        ftp.get(FILENAME, FILENAME.replace('/root', DESTINATION))
        ftp.close()
        ssh.exec_command('rm {}'.format(FILENAME))
        print('OK!')
        return "OK!"
    except:
        print('NOT OK!')
        return 'error'


def _download_file_(DESTINATION):

    print('Connecting to {} as {}... '.format(HOST, USERNAME), flush=True, end='')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(HOST, PORT, USERNAME, PASSWORD)
        print('OK!')
    except:
        print('error')
        exit()


    print('Compressing database... ', flush=True, end='')
    try:
        ssh.exec_command('tar -zcvf {} -C {} .'.format(FILENAME, SOURCE))
        print('OK!')
    except:
        print('error')
        exit()


    print('Downloading file... ', flush=True, end='')
    ftp = ssh.open_sftp()
    try:
        ftp.get(FILENAME, FILENAME.replace('/root', DESTINATION))
        ftp.close()
        print('OK!')
        ssh.exec_command('rm {}'.format(FILENAME))
    except:
        print('error')
    exit()
